'use strict';

var fs = require('fs');
var archiver = require('archiver');
var Promise = require('promise');

module.exports.bundle = function(options) {
    return new Promise((resolve, reject) => {
        if (!options) {
            reject('Missing bundle options.');
            return;
        }

        // TODO -- more value and type checking on options object

        var output = fs.createWriteStream(options.target);
        var archive = archiver('zip');

        output.on('close', () => {
            console.log('Resource bundle created.');
            console.log(archive.pointer() + ' total bytes');
            resolve();
        });

        archive.on('entry', function (data) {
            if (options.verbose) {
                console.log('adding file ' + data.name);
            }
        });

        archive.pipe(output);
        archive.directory(options.source, '');
        archive.finalize();
    });
};